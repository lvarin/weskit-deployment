#  Copyright (c) 2021. Berlin Institute of Health (BIH) and Deutsches Krebsforschungszentrum (DKFZ).
#
#  Distributed under the MIT License. Full text at
#
#      https://gitlab.com/one-touch-pipeline/weskit/api/-/blob/master/LICENSE
#
#  Authors: The WESkit Team

import argparse
import os
import sys
import subprocess
import time
from source.api_demo import create_user, run_demo, get_access_token_from_keycloak

SCHEMA_FILE = "static/user_compose_schema.yaml"
BASE_COMPOSE = "static/weskit_stack_base.yaml"

# 1. process arguments
parser = argparse.ArgumentParser(description="Deploy WESkit services on Docker Swarm.")

subparser = parser.add_subparsers(dest="mode")
start_parser = subparser.add_parser("start")
test_parser = subparser.add_parser("test")

start_parser.add_argument("--compose",
                          help="User compose file extending the base compose file",
                          type=str, required=False)

start_parser.add_argument("--login",
                          help="Activate login management in WESkit and start pre-configured\
                                keycloak server.",
                          action='store_true')

start_parser.add_argument("--minio",
                          help="Add Minio as S3 service.",
                          action='store_true')

test_parser.add_argument("--login",
                         help="Run tests using login.",
                         action='store_true')

args = parser.parse_args()

# hard coded config values
config = {
    "keycloak_host": "http://localhost:8080/auth/realms/WESkit/protocol/openid-connect/token",
    "keycloak_admin_token_url": "http://localhost:8080/auth/realms/master/protocol/openid-connect/token",
    "keycloak_new_user_url": "http://localhost:8080/auth/admin/realms/WESkit/users",
    "keycloak_user_data": {
        "firstName": "T.",
        "lastName": "Est",
        "email": "test@test.test",
        "enabled": "true",
        "username": "test",
        "credentials": [{"type": "password", "value": "test", "temporary": False}]},
    "keycloak_user_credentials": {
        "username": "test",
        "password": "test",
        "client_id": "OTP",
        "client_secret": "7670fd00-9318-44c2-bda3-1a1d2743492d"},
    "keycloak_admin_credentials": {
        "username": "admin",
        "password": "admin",
        "client_id": "admin-cli",
        "grant_type": "password"},
    "weskit_host": "https://localhost",
    "cert": os.path.abspath("certs/weskit.crt")
}

# start new stack
if args.mode == "start":
    base_command = ["docker", "stack", "deploy"]
    compose_files = ["-c", BASE_COMPOSE]
    if args.compose:
        if os.path.isfile(args.compose):
            compose_files += ["-c", args.compose]
        else:
            print("Error: User compose file does not exist.")
            sys.exit()
    if args.login:
        compose_files += ["-c", "static/keycloak.yaml"]
    if args.minio:
        compose_files += ["-c", "static/minio.yaml"]
    try:
        subprocess.run(base_command + compose_files + ["weskit"])
    except Exception:
        print("ERROR: can not run the stack.")
        sys.exit()

# wait and additional configuration
if args.mode == "start" and args.login:
    print("Waiting for running stack")
    time.sleep(25)

    print("Create test user")
    create_user(
        admin_credentials=config["keycloak_admin_credentials"],
        user_data=config["keycloak_user_data"],
        admin_token_url=config["keycloak_admin_token_url"],
        new_user_url=config["keycloak_new_user_url"],
        cert=config["cert"])

if args.mode == "test":
    header = None
    if args.login:
        try:
            print("Run test with user login")
            token = get_access_token_from_keycloak(config["keycloak_host"], config["keycloak_user_credentials"], config["cert"])
            header = dict(Authorization="Bearer " + token["access_token"])
        except Exception:
            print("ERROR: can not reach keycloak. Is keycloak running?")
            print("       Continue without login.")
    try:
        run_demo(config["weskit_host"], config["cert"], header)
    except Exception:
        print("ERROR: can not reach weskit. Is weskit running?")
